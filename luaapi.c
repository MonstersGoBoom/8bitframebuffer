#include <stdlib.h>
#include <luajit.h>
#include <lauxlib.h>
#include <lualib.h>

lua_State *lua;

int lua_error_handler()
{
  printf("Stack = %d\n", lua_gettop(lua));
  printf("Message = %s\n", lua_tostring(lua, -1));
  lua_pop(lua, 1);
  exit(-1);
}

int lua_mypcall( lua_State* L, int nargs, int nret )
{
  int hpos = lua_gettop( L ) - nargs;
  int ret = 0;
 	
  lua_pushcfunction( L, lua_error_handler );
  lua_insert( L, hpos );
  ret = lua_pcall( L, nargs, nret, hpos );
  lua_remove( L, hpos );
  return ret;
}

void lua_callfunc(const char *fname)
{
  lua_getglobal(lua, fname);  /* function to be called */
  lua_mypcall(lua, 0, 0);
}

void app_initialize(const char *fname)
{
	printf("load %s\n",fname);
  lua = luaL_newstate();
  luaJIT_setmode(lua, -1, LUAJIT_MODE_ENGINE|LUAJIT_MODE_ON);
  luaL_openlibs(lua);
  luaL_loadfile(lua,fname);
  lua_mypcall(lua, 0, 0);
}

void app_update()
{
  lua_getglobal(lua, "update");  /* function to be called */
  lua_mypcall(lua, 0, 0);
}


