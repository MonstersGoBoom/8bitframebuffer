
/*
framebuffer
an 8 bit framebuffer sample using WinGDI 
just put a few bits together. 
*/

#define STRICT
#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <mmsystem.h>
#include "framebuffer_win32.h"
#ifndef _STANDALONE_
#include "application.h"
#endif
#define BUFFER_WIDTH 256
#define BUFFER_HEIGHT 256
#define BUFFER_SCALE 2

#define WINDOW_NAME "window to the world"

HINSTANCE g_hInstance;
HWND g_hWnd;
HDC g_hDC;
BITMAPINFO *s_bitmapInfo = NULL;

uint32_t _window_w;
uint32_t _window_h;

//	expose to luajit
__declspec(dllexport) uint8_t s_clut[256*3];
__declspec(dllexport) uint8_t s_pen=255;
__declspec(dllexport) uint8_t *s_buffer;
__declspec(dllexport) uint32_t _buffer_w;
__declspec(dllexport) uint32_t _buffer_h;

// TODO : add keyboard / gamepad 

LRESULT CALLBACK window_procedure (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
		{
				case WM_SIZE:
						_window_w = LOWORD(lParam);
						_window_h = HIWORD(lParam);
						break;
        case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
        default:
            return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
}

// Initialization 

bool system_initialize (int buffer_w,int buffer_h,float scale)
{
    // no WinMain so get the module handle:
    g_hInstance = GetModuleHandle(NULL);
    if (g_hInstance == NULL)
        return false;
    // register the window class:
    WNDCLASS wc;

    wc.style         = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc   = window_procedure;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = g_hInstance;
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = WINDOW_NAME;

    if (RegisterClass(&wc) == 0)
        return false;

		//	create window based on buffer input size , * scale

		_window_w = buffer_w * scale;
		_window_h = buffer_h * scale;
		_buffer_w = buffer_w;
		_buffer_h = buffer_h;
	
		printf("System: %d width %d height\n",_buffer_w,_buffer_h);
		printf("Window: %d width %d height\n",_window_w,_window_h);

    // create the window:
    g_hWnd = CreateWindow(
														WINDOW_NAME,                                             // class name
														WINDOW_NAME,                                             // title
														WS_OVERLAPPED | WS_SIZEBOX | WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_MINIMIZEBOX, // style
														CW_USEDEFAULT, CW_USEDEFAULT,                             // position
														CW_USEDEFAULT, CW_USEDEFAULT,                             // size
														NULL,                                                     // no parent
														NULL,                                                     // no menu
														g_hInstance,                                              // instance
														NULL                                                      // no special
												);

    if (g_hWnd == NULL)
        return false;

    g_hDC = GetDC(g_hWnd);
    if (g_hDC == NULL)
        return false;

//	just make a default grayscale if we're stand alone				
		//	greyscale
		for (int q = 0;q < 256; q++)
		{		
			s_clut[(q*3)+0] = q;
			s_clut[(q*3)+1] = q;
			s_clut[(q*3)+2] = q;
		}

		//	alloc ram for buffer
		s_buffer = (uint8_t*)malloc(_buffer_w*_buffer_h);
		//	clear 		
		memset(s_buffer,0,_buffer_w*_buffer_h);
		//	set up bitmap info 
		s_bitmapInfo = (BITMAPINFO*)malloc(sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 256);
		s_bitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		s_bitmapInfo->bmiHeader.biPlanes = 1;
		s_bitmapInfo->bmiHeader.biBitCount = 8;	//	if you wanted a 1bit or 4bit image this needs to be set and the palette changed
		s_bitmapInfo->bmiHeader.biCompression = BI_RGB;
		s_bitmapInfo->bmiHeader.biWidth = _buffer_w;
		s_bitmapInfo->bmiHeader.biHeight = -_buffer_h;
		s_bitmapInfo->bmiHeader.biClrUsed = 256;
		s_bitmapInfo->bmiHeader.biClrImportant = 256;
    return true;
}

void system_shutdown (UINT uExitCode)
{
	if (g_hDC != NULL)
			ReleaseDC(g_hWnd, g_hDC);

	if (g_hWnd != NULL)
			DestroyWindow(g_hWnd);

	if (s_buffer != NULL)
		free(s_buffer);

	if (s_bitmapInfo != NULL)
		free(s_bitmapInfo);

	WNDCLASS wc;
	if (GetClassInfo(g_hInstance, WINDOW_NAME, &wc) != 0)
			UnregisterClass(WINDOW_NAME, g_hInstance);
}

void system_update ()
{
	MSG msg;
	bool done = false;

	while (!done)
	{
		DWORD dwStart = timeGetTime();
		// poll windows events:
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
				if (msg.message == WM_QUIT)
						done = true;
				TranslateMessage(&msg);
				DispatchMessage(&msg);
		}

#ifdef _STANDALONE_
//	show random
		for (int i = 0; i < _buffer_w * _buffer_h; ++i)
		{
			s_buffer[i] = rand();	
		}
#else
		app_update();
#endif
		//	copy palette to the dib 
		for (int q = 0;q < 256; q++)
		{		
			s_bitmapInfo->bmiColors[q].rgbRed 		= s_clut[(q*3)+0]; 
			s_bitmapInfo->bmiColors[q].rgbGreen 	= s_clut[(q*3)+1]; 
			s_bitmapInfo->bmiColors[q].rgbBlue 		= s_clut[(q*3)+2]; 
		}
		// blit image
		StretchDIBits(g_hDC, 0, 0, _window_w, _window_h, 0, 0, _buffer_w, _buffer_h, s_buffer, s_bitmapInfo, DIB_RGB_COLORS, SRCCOPY);
		
		// have a nap 
		DWORD dwDelta = timeGetTime() - dwStart;
		srand(timeGetTime());
		if (dwDelta < STEP_RATE)
		{
				Sleep(STEP_RATE - dwDelta);
		}
	}
}

// A helper to resize the window with respect to the client area:
void windows_resize_client (HWND hWnd, UINT uWidth, UINT uHeight)
{
	RECT rcClient, rcWindow;
	GetClientRect(hWnd, &rcClient);
	GetWindowRect(hWnd, &rcWindow);
	MoveWindow(hWnd,
							rcWindow.left,
							rcWindow.top,
							uWidth + (rcWindow.right - rcWindow.left) - rcClient.right,
							uHeight + (rcWindow.bottom - rcWindow.top) - rcClient.bottom,
							FALSE);
}

int main(int argc,char *argv[])
{
	if (!system_initialize(BUFFER_WIDTH,BUFFER_HEIGHT,BUFFER_SCALE))
	{
			fprintf(stderr,"Initialization failed.");
			system_shutdown(1);
	}

#ifndef _STANDALONE_
	if (argc==2)
		app_initialize(argv[1]);
	else
	{
		printf("%s filename",argv[0]);
		system_shutdown(0);
		exit(0);
	}		
#endif	
	windows_resize_client(g_hWnd, _window_w, _window_h);
	ShowWindow(g_hWnd, SW_SHOW);

	system_update();
	system_shutdown(0);
}

