
ffi = require("ffi")
-- FFI CDEFS
ffi.cdef[[
unsigned char s_clut[];
unsigned char *s_buffer;
unsigned char s_pen;
unsigned int 	_buffer_w;
unsigned int 	_buffer_h;
int 					puts_xy(const char *s,int x, int y);
]]

clut = ffi.C.s_clut;
vram = ffi.C.s_buffer;
vramwidth = ffi.C._buffer_w;
vramheight = ffi.C._buffer_h;

--	set random colors , leave 0 and 255 alone ( black & white )

for c=1,254 do
	clut[(c*3)+0] = math.random(255)
	clut[(c*3)+1] = math.random(255)
	clut[(c*3)+2] = math.random(255)
end

local function printxy(str,x,y,col)
	ffi.C.s_pen = col
	ffi.C.puts_xy(str,x,y);
end

local function pix(x,y,c)
	if x>=0 and x<vramwidth and y>=0 and y<vramheight then 
		vram[x+(y*vramwidth)]=c
	end
end

local function hline(x,y,x1,color)
-- sanity 
-- if Y off then skip	
	if y<0 or y>=vramheight then return end
-- if backwards then skip	
	if (x>x1) then return	end
	if x<0 then x=0; end
	if x1>=vramwidth then x1=vramwidth; end
	local addr = vram + (x+(y*vramwidth))
	ffi.fill( addr , x1-x , color)	
end

local function rect(x,y,w,h,c)
	for cy=0,h do
		hline(x,y+cy,x+w,c)
	end
end

function circ( sx,  sy,  radius,  color)
	r = radius;
	x = -r
	y = 0
	err = 2-2*r
	while (x<0) do
		hline(sx+x,sy+y,sx-x,color)
		hline(sx+x,sy-y,sx-x,color)
		r = err
		if (r <= y) then 
			y=y+1
			err = err + y*2+1
		end
		if (r > x or err > y) then
			x=x+1
			err = err+x*2+1;
		end
	end
end

function circb( sx,  sy,  radius,  color)
	r = radius;
	x = -r
	y = 0
	err = 2-2*r
	while (x<=0) do
		pix(sx-x,sy-y,color)
		pix(sx+x,sy-y,color)
		pix(sx-x,sy+y,color)
		pix(sx+x,sy+y,color)
		r = err
		if (r <= y) then 
			y=y+1
			err = err + y*2+1
		end
		if (r > x or err > y) then
			x=x+1
			err = err+x*2+1;
		end
	end
end

local t = 0.0
function update()
	rect(0,0,239,135,math.floor(t))
	t=t+1
	rect(0,0,64,64,20)
	for y=0,vramheight do
		for x=0,vramwidth do 
			pix(x,y,math.random( 255 ))
		end
	end
	circ(32,32,t%129,200)
	circb(120,96,t%200,255)
	printxy("Hello There!",0,0,255)
	printxy("Hello There!",t%vramwidth,64,255)
end
