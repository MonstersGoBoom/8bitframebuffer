

solution "8BitFrameBuffer"
--------------------------------------------------------------------------
									configurations    { "Debug", "Release" }
									targetdir           "bin"
									debugdir            "bin"
									configuration 		"Debug"
									defines           { "DEBUG", "_CRT_SECURE_NO_WARNINGS", "_WINSOCK_DEPRECATED_NO_WARNINGS" }
									flags             { "Symbols" }
									configuration 		"Release"
									defines           { "NDEBUG", "_CRT_SECURE_NO_WARNINGS", "_WINSOCK_DEPRECATED_NO_WARNINGS" }
									flags             { "OptimizeSize" }
--------------------------------------------------------------------------
project 					"simple"
									kind            "ConsoleApp"
									language        "C"
									objdir          "_build"
									defines					{ "_STANDALONE_"}
									flags           { "FatalWarnings", "NoExceptions", "NoRTTI", "WinMain" }
									files           { "framebuffer_win32.c"}
									links           { "gdi32" , "winmm" }

project 					"simple_luajit"
									kind            "ConsoleApp"
									language        "C"
									objdir          "_build"
									defines					{ }
									flags           { "FatalWarnings", "NoExceptions", "NoRTTI", "WinMain" }
									includedirs     { "luajit-2.0.5/include" }
									files           { "framebuffer_win32.c","luaapi.c","font.c"}
									libdirs         { "luajit-2.0.5/" }
									links           { "gdi32" , "winmm", "luajit" }
--------------------------------------------------------------------------
      