# 8bitframebuffer
## Simple indexed framebuffer sample 

simple.exe will fill the buffer with noise .

simple_lua.exe will load supplied lua file and allow it to draw to the buffer via FFI.

### for example
simple_lua.exe cart.lua 

will show noise , text , and circles draw via Lua 

### To Build
use https://github.com/bkaradzic/GENie 
I use ```genie gmake``` then type ```make config=release```




