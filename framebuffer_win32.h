#include <stdbool.h>
#include <stdint.h>

#define STEP_RATE 13

extern uint8_t s_clut[256*3];
extern uint8_t *s_buffer;
extern uint8_t s_pen;
extern uint32_t _buffer_w;
extern uint32_t _buffer_h;

static inline void _pixel(uint32_t x,uint32_t y,uint8_t color)
{
	s_buffer[x+(y*_buffer_w)]=color;
}

